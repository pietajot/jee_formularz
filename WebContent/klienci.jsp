<%@ page import="java.util.*" import="pl.kurs.model.Klient"%>

<%@ include file="naglowek.jsp"%>

<div class="container">

	<div class="panel panel-default">
		<div class="panel-heading">Tutaj pojawia sie moi klienci</div>
		<%
			if (request.getAttribute("blad") != null) {
		%>

		<div class="alert alert-danger">
			<%=request.getAttribute("blad")%>
		</div>

		<%
			}
		%>
		<table class="panel-body table table-striped">
			<tr>
				<th>Imie</th>
				<th>Nazwisko</th>
				<th>Identyfikator</th>
				<th>Typ klienta</th>
				<th>Akcje</th>
			</tr>

			<%
				for (Klient k : (ArrayList<Klient>) request.getAttribute("klienci")) {
			%>
			<tr>
				<td><%=k.getImie()%></td>
				<td><%=k.getNazwisko()%></td>
				<td>
					<%=k.getIdentyfikator() %>
				</td>
				<td>
					<% if(k.isCzyFirma()) { %>
						Firma
					<% } else { %>
						Osoba
					<% } %>
				</td>
				
				<td>
					<form action="<%= request.getContextPath() %>/" method="post">
						<input type="hidden" name="numerId" value="<%= k.getId() %>" />
						<input type="submit" 
							value="Usun" 
							class="btn btn-danger" 
							/>
					</form>
					
					<a href="<%= request.getContextPath() %>/formularz?id=<%= k.getId() %>" class="btn btn-success">Edytuj</a>
				</td>
			</tr>
			<%
				}
			%>

		</table>
		<div class="panel-footer">
			<a href="<%= request.getContextPath() %>/formularz">Dodaj nowy</a>
		</div>

	</div>
</div>

</div>
<%@ include file="stopka.jsp"%>