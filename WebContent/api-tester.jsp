<%@ include file="naglowek.jsp"%>

<style>
#resp_content {
	-ms-word-break: break-all;
	word-break: break-all;
	/* Non standard for webkit */
	word-break: break-word;
	-webkit-hyphens: auto;
	-moz-hyphens: auto;
	-ms-hyphens: auto;
	hyphens: auto;
}
</style>

<div class="container">
	<div class="col-sm-6">
		<div class="panel panel-default">
			<div class="panel-heading">Tester API - parametry</div>
			<div class="panel-body">
				<div class="form-group">
					<label for="req_url">Adres URL</label><input type="text"
						class="form-control" id="req_url"
						value="http://localhost:8080/Formularz/rest/klienci" />
				</div>
				<div class="form-group">
					<label for="req_json">JSON</label>
					<textarea id="req_json" name="req_json" class="form-control"></textarea>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input type="radio" name="req_method" value="GET"
							id="req_method_get" checked="checked" /> <label
							for="req_method_get"> GET</label>
					</div>
					<div class="form-check">
						<input type="radio" name="req_method" value="POST"
							id="req_method_post" /> <label for="req_method_post">
							POST</label>
					</div>

					<div class="form-check">
						<input type="radio" name="req_method" value="PUT"
							id="req_method_put" /> <label for="req_method_put"> PUT</label>
					</div>
					<div class="form-check">
						<input type="radio" name="req_method" value="DELETE"
							id="req_method_delete" /> <label for="req_method_delete">
							DELETE</label>
					</div>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Wyslij"
						onclick="sendTestXMLHttpRequest()" />
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="panel panel-default">
			<div class="panel-heading">Wynik</div>
			<table class="panel-body table">
				<tr>
					<th>Status</th>
					<td id="resp_status"></td>
				</tr>
				<tr>
					<th>Naglowki</th>
					<td id="resp_headers"></td>
				</tr>
				<tr>
					<th>Zawartosc</th>
					<td id="resp_content"></td>
				</tr>

			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	function sendTestXMLHttpRequest() {

		// pobieramy wartość pola URL
		var url = document.getElementById("req_url").value;
		if (url === "") {
			alert("Prosze wypelnic pole URL!!!");
			return;
		}

		// pobieramy wartość wybranego radio buttona i przypisujemy ją do zmiennej method
		var radios = document.getElementsByName('req_method');
		var method = "GET";

		for (var i = 0, length = radios.length; i < length; i++) {
			if (radios[i].checked) {
				var mv = radios[i].value;
				if (mv == "POST" || mv == "PUT" || mv == "DELETE"
						|| mv == "GET") {
					method = mv;
				}
				break;
			}
		}

		// wykonujemy asynchroniczne zapytanie XMLHttpRequest
		var xhr = new XMLHttpRequest();
		xhr.open(method, url);

		// sprawdzamy czy w polu json cos jest
		var jsonFieldVal = document.getElementById("req_json").value;

		if (jsonFieldVal.length > 0) {
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.send(jsonFieldVal);
		}

		xhr.onload = function() {
			document.getElementById("resp_status").innerHTML = xhr.status;
			document.getElementById("resp_headers").innerHTML = xhr
					.getAllResponseHeaders();
			document.getElementById("resp_content").innerHTML = xhr.responseText;
		};
		xhr.send();
	}
</script>


<%@ include file="stopka.jsp"%>