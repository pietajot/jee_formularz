<%@ include file="naglowek.jsp"%>

<div class="container">

	<%
		if (request.getAttribute("blad") != null) {
	%>
	<div class="alert alert-danger">
		<%=request.getAttribute("blad")%>
	</div>
	<%
		}
	%>

	<%
		if (request.getAttribute("sukces") != null) {
	%>
	<div class="alert alert-success">
		<%=request.getAttribute("sukces")%>
	</div>
	<%
		}
	%>


	<div class="panel panel-default">
		<div class="panel-heading">Wprowadzanie danych o kliencie</div>
		<div class="panel-body">
			<form action="http://localhost:8080/Formularz/formularz"
				method="post">

				<%
					if (request.getAttribute("id") != null) {
				%>
				<input type="hidden" value="<%=request.getAttribute("id")%>"
					name="id" />
				<%
					}
				%>

				<div class="form-group">
					<label for="imie" class="col-sm-2">Imie</label> <input type="text"
						<%if (request.getAttribute("imie") != null) {%>
						value="<%=request.getAttribute("imie")%>" <%}%> name="imie"
						id="imie" />
				</div>
				<div class="form-group">
					<label for="nazwisko" class="col-sm-2">Nazwisko</label> <input type="text"
						<%if (request.getAttribute("nazwisko") != null) {%>
						value="<%=request.getAttribute("nazwisko")%>" <%}%>
						name="nazwisko" id="nazwisko" />

				</div>

				<div class="form-group">
					<label for="identyfikator" class="col-sm-2">Pesel / NIP</label> 
					
					<input type="text"
						<%if (request.getAttribute("identyfikator") != null) {%>
						value="<%=request.getAttribute("identyfikator")%>" <%}%>
						name="identyfikator" id="identyfikator" />

				</div>
				<div class="form-group">

					<input type="checkbox" name="czy_firma" id="czy_firma" value="1"
						<%if (request.getAttribute("czy_firma") != null) {%>
						checked="checked" <%}%> /> <label for="czy_firma">
						Klient instytucjonalny</label>

				</div>



				<div class="form-group">
					<input type="submit" value="wyslij" class="btn btn-primary" />
				</div>

				<a class="btn btn-danger pull-right" href="<%=request.getContextPath()%>/Formularz">Powrot</a>
			</form>
		</div>
	</div>


	
</div>


<%@ include file="stopka.jsp"%>