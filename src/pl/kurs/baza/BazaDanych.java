package pl.kurs.baza;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BazaDanych {

	// moja baza do kt�rej chce si� po��czy�
	static String bazaDanych = "jee_zajecia";
	// u�ytkownik mysql kt�ry po��czy si� z baz�
	static String uzytkownik = "jee_zajecia";
	// has�o u�ytkownika
	static String haslo = "jee_zajecia";
	// tzw. connection string :)
	static String url = "jdbc:mysql://localhost:3306/" + bazaDanych + "?useSSL=false&serverTimezone=UTC&verifyServerCertificate=false&allowPublicKeyRetrieval=true";
	
	// singleto-pattern polaczenie
	private static Connection polaczenie;
	
	public static Connection polacz() throws SQLException, ClassNotFoundException
	{
		if (polaczenie != null) {
			return polaczenie;
		}
		Class.forName("com.mysql.jdbc.Driver");
		polaczenie = DriverManager.getConnection(url,uzytkownik,haslo);
		
		return polaczenie;
	}
	
}
