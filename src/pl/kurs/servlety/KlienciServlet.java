package pl.kurs.servlety;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.kurs.baza.BazaDanych;
import pl.kurs.model.Klient;

/**
 * Servlet implementation class KlienciServlet
 */
@WebServlet("/")
public class KlienciServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KlienciServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		ArrayList<Klient> klienci;
		try {
			Connection polaczenie = BazaDanych.polacz();
			
			klienci = Klient.pobierzWszystkichZBazy(polaczenie);
			
		} catch (ClassNotFoundException | SQLException e) {
			klienci = new ArrayList<Klient>();
			request.setAttribute("blad", "Blad polaczenia z baza danych");
			e.printStackTrace();
		}
		
		request.setAttribute("klienci", klienci);
		
		request.getRequestDispatcher("klienci.jsp")
			.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String paramId = request.getParameter("numerId");
		
		int id = 0;
		
		if (paramId != null) {
			id = Integer.parseInt(paramId);
		}
		
		if (id > 0) {
			try {
				Connection polaczenie = BazaDanych.polacz();
			
				String sql = "DELETE FROM klienci WHERE id = ?;";
			
				PreparedStatement usuwanie = polaczenie.prepareStatement(sql);
				
				usuwanie.setInt(1, id);
				
				usuwanie.executeUpdate();
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
		
		doGet(request, response);
	}

}
