package pl.kurs.servlety;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.kurs.baza.BazaDanych;
import pl.kurs.model.Klient;

/**
 * Servlet implementation class FormularzServlet
 */
@WebServlet("/formularz")
public class FormularzServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FormularzServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String paramId = request.getParameter("id");

		if (paramId != null) {
			try {
				int id = Integer.parseInt(paramId);
				Connection polaczenie = BazaDanych.polacz();

				Klient k = new Klient();
				k.setId(id);
				k.wypelnijZBazy(polaczenie);

				request.setAttribute("id", k.getId());
				request.setAttribute("imie", k.getImie());
				request.setAttribute("nazwisko", k.getNazwisko());
				request.setAttribute("identyfikator", k.getIdentyfikator());
				if(k.isCzyFirma())
					request.setAttribute("czy_firma", k.isCzyFirma());

			} catch (Exception e) {
				request.setAttribute("blad", "Szukany klient nie istnieje");
				e.printStackTrace();
			}
		}

		request.getRequestDispatcher("formularz.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String imie = request.getParameter("imie");

		String nazwisko = request.getParameter("nazwisko");

		String identyfikator = request.getParameter("identyfikator");
		
		String paramCzyFirma = request.getParameter("czy_firma");
		
		boolean czyFirma = false;
		if(paramCzyFirma != null && paramCzyFirma.equals("1")) {
			czyFirma = true;
		}
		
		String wiadomoscOBledzie = null;
		String wiadomoscOSukcesie = null;

		if (imie == null || imie == "") {
			wiadomoscOBledzie = "Brak imienia!!";
		} else if (nazwisko == null || nazwisko == "") {
			wiadomoscOBledzie = "Brak nazwiska!!!";
		} else {

			String paramId = request.getParameter("id");

			// tutaj edytujemy
			if (paramId != null) {
				try {
					int id = Integer.parseInt(paramId);

					Connection polaczenie = BazaDanych.polacz();
					String sql = "UPDATE klienci SET imie=?, nazwisko=?, "
							+ "identyfikator=?, czy_firma=? WHERE id=?";

					PreparedStatement wsad = polaczenie.prepareStatement(sql);

					wsad.setString(1, imie);
					wsad.setString(2, nazwisko);
					wsad.setString(3, identyfikator);
					wsad.setBoolean(4, czyFirma);
					wsad.setInt(5, id);

					wsad.executeUpdate();
					wiadomoscOSukcesie = "Pomyslnie zaktualizowano klienta";

				} catch (Exception e) {
					wiadomoscOBledzie = "Wydarzylo sie cos dziwnego";
				}

			} else {
				Klient k = new Klient();
				k.setImie(imie);
				k.setNazwisko(nazwisko);

				Connection polaczenie;
				try {
					polaczenie = BazaDanych.polacz();
					String sql = "INSERT INTO klienci(imie, nazwisko, identyfikator, czy_firma) "
							+ "VALUES (?, ?, ?, ?) ";

					PreparedStatement wsad = polaczenie.prepareStatement(sql);
					wsad.setString(1, imie);
					wsad.setString(2, nazwisko);
					wsad.setString(3, identyfikator);
					wsad.setBoolean(4, czyFirma);
					
					wsad.executeUpdate();

				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				wiadomoscOSukcesie = "Poprawnie dodano klienta";
			}
			imie = null;
			nazwisko = null;
		}

		request.setAttribute("blad", wiadomoscOBledzie);
		request.setAttribute("sukces", wiadomoscOSukcesie);
		request.setAttribute("imie", imie);
		request.setAttribute("nazwisko", nazwisko);
		request.getRequestDispatcher("formularz.jsp").include(request, response);
	}

}
