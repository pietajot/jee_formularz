package pl.kurs.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class Klient {

	@Expose
	private int id;
	
	@Expose
	private String imie, nazwisko;
	
	@Expose
	private String identyfikator;
	
	@Expose
	private boolean czyFirma;

	public Klient()
	{
		
	}
	
	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdentyfikator() {
		return identyfikator;
	}

	public void setIdentyfikator(String identyfikator) {
		this.identyfikator = identyfikator;
	}

	public boolean isCzyFirma() {
		return czyFirma;
	}

	public void setCzyFirma(boolean czyFirma) {
		this.czyFirma = czyFirma;
	}
	
	/**
	 * Polacz z baza danych i wypelnij wszytkie pola obiektu na podstawie id obiektu
	 * 
	 * @param polaczenie
	 * @throws SQLException 
	 */
	public void wypelnijZBazy(Connection polaczenie) throws SQLException
	{
		if (id > 0)
		{
			String sql = "SELECT id, imie, nazwisko, " 
					+ "identyfikator, czy_firma FROM klienci WHERE id = ?";

			PreparedStatement zapytanie = polaczenie.prepareStatement(sql);

			zapytanie.setInt(1, id);

			ResultSet wynik = zapytanie.executeQuery();
			
			int iter = 0;
			while (wynik.next()) {
				setId(wynik.getInt("id"));
				setImie(wynik.getString("imie"));
				setNazwisko(wynik.getString("nazwisko"));
				setIdentyfikator(wynik.getString("identyfikator"));
				setCzyFirma(wynik.getBoolean("czy_firma"));
				iter ++;
			}
			
			if (iter == 0) {
				this.setId(0);
			}
			
		}	
	}
	
	public static ArrayList<Klient> pobierzWszystkichZBazy(Connection polaczenie) throws SQLException
	{
		ArrayList<Klient> klienci = new ArrayList<Klient>();
		
		String sql = "SELECT id, imie, "
				+ "nazwisko, identyfikator, "
				+ "czy_firma FROM klienci;";
	
		PreparedStatement zapytanie = polaczenie.prepareStatement(sql);
		
		ResultSet wynikZapytania = zapytanie.executeQuery();
		
		while(wynikZapytania.next()) {
			Klient k = new Klient();
			k.setId(wynikZapytania.getInt("id"));
			k.setImie(wynikZapytania.getString("imie"));
			k.setNazwisko(wynikZapytania.getString("nazwisko"));
			k.setIdentyfikator(wynikZapytania.getString("identyfikator"));
			k.setCzyFirma(wynikZapytania.getBoolean("czy_firma"));
			klienci.add(k);
		}
		
		return klienci;
	}
	
}
