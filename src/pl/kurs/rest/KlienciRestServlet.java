package pl.kurs.rest;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.MalformedJsonException;

import pl.kurs.baza.BazaDanych;
import pl.kurs.model.Klient;

/**
 * Servlet implementation class KlienciRestServlet
 */
@WebServlet("/rest/klienci")
public class KlienciRestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id");

		int iId = 0;

		String json = "";

		response.setHeader("Content-Type", "application/json");

		PrintWriter pisacz = response.getWriter();

		if (id != null) {
			try {

				iId = Integer.parseInt(id);

			} catch (NumberFormatException e) {

				response.setStatus(Response.SC_BAD_REQUEST);

				HashMap<String, String> bledy = new HashMap<>();

				bledy.put("wiadomosc", "Niepoprawny format parametru ID");

				Gson gson = new Gson();

				json = gson.toJson(bledy);

				pisacz.write(json);

				return;
			}
		}

		try {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

			if (iId > 0) {
				Klient k = new Klient();

				k.setId(iId);

				k.wypelnijZBazy(BazaDanych.polacz());

				if (k.getId() > 0) {
					json = gson.toJson(k);
				} else {
					HashMap<String, String> bledy = new HashMap<>();

					bledy.put("wiadomosc", "Klient nie znaleziony");

					json = gson.toJson(bledy);

					response.setStatus(Response.SC_NOT_FOUND);
				}

			} else {
				ArrayList<Klient> klienci = Klient.pobierzWszystkichZBazy(BazaDanych.polacz());

				json = gson.toJson(klienci);
			}
		} catch (ClassNotFoundException | SQLException e) {

			HashMap<String, String> bledy = new HashMap<>();

			bledy.put("wiadomosc", "Wystapil blad w pobieraniu klientow");

			Gson gson = new Gson();

			json = gson.toJson(bledy);

			response.setStatus(Response.SC_BAD_REQUEST);

		}

		pisacz.write(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String jsonWejsciowy = "";
		Klient klient;
		String json = "";
		
		String linia;
		while((linia = request.getReader().readLine()) != null) {
			jsonWejsciowy += linia;
		}
		
		PrintWriter pisacz = response.getWriter();
		
		Gson gson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation()
				.create();
		
		try {
			
			klient = gson.fromJson(jsonWejsciowy, Klient.class);
	
		}catch (Exception e) {
			HashMap<String, String> bledy = new HashMap<>();

			bledy.put("wiadomosc", "Niepoprawny format JSON przeslanych danych");

			gson = new Gson();

			json = gson.toJson(bledy);

			response.setStatus(Response.SC_BAD_REQUEST);
			
			pisacz.println(json);
			
			return;
		}
		
		Connection polaczenie;
		
		try {
			polaczenie = BazaDanych.polacz();
			String sql = "INSERT INTO klienci(imie, nazwisko, identyfikator, czy_firma) "
					+ "VALUES (?, ?, ?, ?) ";

			PreparedStatement wsad = polaczenie.prepareStatement(sql);
			wsad.setString(1, klient.getImie());
			wsad.setString(2, klient.getNazwisko());
			wsad.setString(3, klient.getIdentyfikator());
			wsad.setBoolean(4, klient.isCzyFirma());
			
			wsad.executeUpdate();
			
			response.setStatus(Response.SC_CREATED);

			json = gson.toJson(klient);
			
		} catch (ClassNotFoundException | SQLException e) {

			HashMap<String, String> bledy = new HashMap<>();

			bledy.put("wiadomosc", "Wystapil blad polaczenia z baza danych");

			gson = new Gson();

			json = gson.toJson(bledy);

			response.setStatus(Response.SC_BAD_REQUEST);

		}
		
		pisacz.println(json);
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().write("moj servlet dziala i dostal PUT !!");
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String id = request.getParameter("id");

		int iId = 0;

		String json = "";

		response.setHeader("Content-Type", "application/json");

		PrintWriter pisacz = response.getWriter();

		if (id != null) {
			try {

				iId = Integer.parseInt(id);

				try {
					Connection polaczenie = BazaDanych.polacz();
				
					String sql = "DELETE FROM klienci WHERE id = ?;";
				
					PreparedStatement usuwanie = polaczenie.prepareStatement(sql);
					
					usuwanie.setInt(1, iId);
					
					int wynik = usuwanie.executeUpdate();
					
					if (wynik > 0) {
						
						response.setStatus(Response.SC_NO_CONTENT);
						
					} else {
						
						response.setStatus(Response.SC_NOT_FOUND);
						
					}
					
				} catch (ClassNotFoundException | SQLException e) {
					
					HashMap<String, String> bledy = new HashMap<>();

					bledy.put("wiadomosc", "Nie udalo sie przeprowadzic usuwania ze wzgledu na blad polaczenia z baza danych");

					Gson gson = new Gson();

					json = gson.toJson(bledy);

					response.setStatus(Response.SC_BAD_REQUEST);
				}
				
			} catch (NumberFormatException e) {

				response.setStatus(Response.SC_BAD_REQUEST);

				HashMap<String, String> bledy = new HashMap<>();

				bledy.put("wiadomosc", "Niepoprawny format parametru ID");

				Gson gson = new Gson();

				json = gson.toJson(bledy);
			}
		} else {
			response.setStatus(Response.SC_BAD_REQUEST);

			HashMap<String, String> bledy = new HashMap<>();

			bledy.put("wiadomosc", "Brak wymaganego parametru ID");

			Gson gson = new Gson();

			json = gson.toJson(bledy);
		}
		pisacz.write(json);

	}
}
